import numpy as np
from lmfit import minimize, Parameters, Minimizer, report_fit


def cmp(params, SA, IA, Fz, data):
    a0 = params['a0']
    a1 = params['a1']
    a2 = params['a2']
    a3 = params['a3']
    a4 = params['a4']
    a5 = params['a5']
    a6 = params['a6']
    a7 = params['a7']
    a8 = params['a8']
    a9 = params['a9']
    a10 = params['a10']
    a11 = params['a11']
    a12 = params['a12']
    a13 = params['a13']
    a14 = params['a14']
    a15 = params['a15']
    a16 = params['a16']
    a17 = params['a17']

    C = a0
    D = Fz * (a1 * Fz + a2) * (1 - a15 * IA * IA)
    BCD = a3 * np.sin(np.arctan(Fz / a4) * 2) * (1 - a5 * np.abs(IA))
    B = BCD / (C * D)
    H = a8 * Fz + a9 + a10 * IA
    E = (a6 * Fz + a7) * (1 - (a16 * IA + a17) * np.sign(SA + H))
    V = a11 * Fz + a12 + (a13 * Fz + a14) * IA * Fz
    Bx1 = B * (SA + H)

    model = D * np.sin(C * np.arctan(Bx1 - E * (Bx1 - np.arctan(Bx1)))) + V
    #print(model[0]-data[0])
    return model - data


def fnc2min(model, Fz, SA, IA, data):
    # create a set of Parameters
    params = Parameters()
    params.add("a0", value=0, min=1.2, max=18)
    params.add("a1", value=0, min=-80, max=80)
    params.add("a2", value=0, min=900, max=1700)
    params.add("a3", value=0, min=500, max=2000)
    params.add("a4", value=0, min=0, max=50)
    params.add("a5", value=0, min=-0.1, max=0.1)
    params.add("a6", value=0, min=-2, max=2)
    params.add("a7", value=0, min=-20, max=1)
    params.add("a8", value=0, min=-1, max=1)
    params.add("a9", value=0, min=-1, max=1)
    params.add("a10", value=0, min=-0.1, max=0.1)
    params.add("a11", value=0, min=-200, max=200)
    params.add("a12", value=0, min=-10, max=10)
    params.add("a13", value=0, min=-10, max=10)
    params.add("a14", value=0, min=-15, max=15)
    params.add("a15", value=0, min=-0.01, max=0.01)
    params.add("a16", value=0, min=-0.1, max=0.1)
    params.add("a17", value=0, min=-1, max=1)

    # do fit, here with the default leastsq algorithm
    minner = Minimizer(cmp, params, fcn_args=(SA, IA, Fz, data), ftol=0.00002)
    result = minner.minimize()

    # calculate final result
    #final = data + result.residual

    # write error report
    report_fit(result)

    for v in result.params:
        setattr(model, v, result.params[v].value)

    return model


def fit_lateral(model, Fz, SA, IA, data):
    return fnc2min(model=model, Fz=Fz, SA=SA, IA=IA, data=data)
