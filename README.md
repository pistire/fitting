# PisTire

Project that allows the user to create, fit and plot pacejka tire models.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

```
ISaveLoad needs to be added to the project repository
```


## Using the project

Open main.py to see how to create a model, fit and plot.


## Authors

* **Joao Antunes** - *Initial work* - [Gitlab](https://gitlab.com/JoaoPedroAntunes)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

